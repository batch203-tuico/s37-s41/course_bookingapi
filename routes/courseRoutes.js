const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

console.log(courseControllers);

// COURSE CREATION
router.post("/", auth.verify, courseControllers.addCourse);

// RETRIEVE ALL COURSES (ADMIN ONLY)
router.get("/all", auth.verify, courseControllers.getAllCourses);

// RETRIEVE ALL ACTIVE COURSES (ALL USER)
router.get("/", courseControllers.getAllActiveCourses);

// RETRIEVE A COURSE
router.get("/:courseId", courseControllers.getCourse);

// UPDATE A COURSE
router.put("/:courseId", auth.verify, courseControllers.updateCourse);

// ARCHIVE A COURSE BY CHANGING THE STATUS TO FALSE
router.patch("/archive/:courseId", auth.verify, courseControllers.archiveCourse);















module.exports = router;