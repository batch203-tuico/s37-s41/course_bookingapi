const Course = require("../models/Course");
const auth = require("../auth");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/

// COURSE CREATION
// module.exports.addCourse = (req, res) => {
//     // Create a variable and instantiates the "Course" model
//     let newCourse = new Course({
//         name: req.body.name,
//         description: req.body.description,
//         price: req.body.price,
//         slots: req.body.slots
//     })

//     // console.log(newCourse);

//     newCourse.save()
//     // Course created successfully
//     .then(course => {
//         console.log(course);
//         res.send(true);
//     })
//     // Course not saved
//     .catch(error => {
//         console.log(error);
//         res.send(false);
//     })
// };
//====================================================
/* 
    s39 Activity:
    1. Refactor the course route to implement user authentication for the admin when creating a course.
    2. Refactor the addCourse controller method to implement admin authentication for creating a course.
    4. Update your remote link and push to git with the commit message of Add activity code - S39.
    5. Add the link in Boodle.
*/

// COURSE CREATION IF ADMIN
module.exports.addCourse = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);

    // Create a variable and instantiates the "Course" model
    let newCourse = new Course({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        slots: req.body.slots
        });

    // console.log(newCourse);
    
    if (userData.isAdmin){
        
        newCourse.save()
        // Course created successfully
        .then(course => {
            console.log(course);
            res.send(true);
        })
        // Course not saved
        .catch(error => {
            console.log(error);
            res.send(false);
        })
    } else {
        return res.status(401).send("You don't have access to this page!")
    }
};
//====================================================

/* 
    Steps:
        1. Retrieve all the courses (active/inactive) from the database.
        2. Verify the role of the current user (Admin).
*/

// RETRIEVE ALL COURSES
module.exports.getAllCourses = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);

    if (userData.isAdmin) {
        return Course.find({})
        .then(result => res.send(result));

    } else {
        // return res.send(false);
        return res.status(401).send("You don't have access to this page!")
    }
};
//====================================================

/* 
    Step:
        1. Retrieve all the courses from the database with the property "isActive" true.
*/

// RETRIEVE ALL ACTIVE COURSES (ALL USERS)
module.exports.getAllActiveCourses = (req, res) => {
    return Course.find({isActive:true})
    .then(result => res.send(result))
};
//====================================================


/* 
    Step:
        1. Retrieve the course that matches the course ID provided from the URl
*/

// RETRIEVE A SPECIFIC COURSE
module.exports.getCourse = (req, res) => {
    console.log(req.params.courseId);

    return Course.findById(req.params.courseId)
    .then(result => res.send(result));
};
//====================================================

/* 
    Steps:
        1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	    2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body.
*/

// UPDATE A COURSE (ADMIN ONLY)
module.exports.updateCourse = (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
        let updatedCourse = {
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            slots: req.body.slots
        };
        console.log(userData);

        // Syntax
            // findByIdAndUpdate(documentId, updatesToBeApplied, {new:true});
        return Course.findByIdAndUpdate(req.params.courseId, updatedCourse, {new:true}) //use to display the updated result

        .then(result => {
            console.log(result);
            res.send(result);
        })
        .catch(error => {
            console.log(error);
            res.send(false);
        })
    } else {
        return res.status(401).send("You don't have access to this page!")
    }
};
//====================================================

// Archive a course
    // Soft delete happens when a course status (isActive) is set to false.

module.exports.archiveCourse = (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    let updateIsActiveField = {
        isActive: req.body.isActive
    };

    if (userData.isAdmin) {
        return Course.findByIdAndUpdate(req.params.courseId, updateIsActiveField, {new:true})

        .then(result => {
            console.log(result);
            res.send(true);
        })
        .catch(error => {
            console.log(error);
            res.send(false);
        })
    } else {
        return res.status(401).send("You don't have access to this page!")
    }
};
//====================================================
