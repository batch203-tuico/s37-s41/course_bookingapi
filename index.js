// packages
const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// Allows our backend application to be available with our frontend application
// Cross Origin Resource Sharing (CORS)
const cors = require("cors");

const app = express();

// port
// this syntax will allow flexibility when using the application locally or as hosted app.
const port = process.env.PORT || 4000;

// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());


// mongoDB connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.7jqbank.mongodb.net/batch203_bookingAPI?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;

// Captures and give details of the error when connecting to mongoose
db.on("error", console.error.bind(console, "connection error"));

// Successful connection and can made queries
db.once("open", () => console.log("Connected nga, wala namang KAYO :("));


// USER ROUTES
// Defines the "/users" string to be included for all user routes in the "userRoutes"
app.use("/users", userRoutes)

// COURSE ROUTES
// "/courses" string will be included for all course routes inside the "courseRoutes" file.
app.use("/courses", courseRoutes);







app.listen(port, () => {
    console.log(`API is now online on port ${port}`)
});