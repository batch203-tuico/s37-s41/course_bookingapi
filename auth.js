const jwt = require("jsonwebtoken");
const User = require("./models/User");

// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret key.
const secret = "CourseBookingAPI";


// [SECTION] JSON Web Tokens
    // - JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
    // - Information is kept in

// Token Creation
/* 
    Analogy:
        Pack the gift provided with a provided lock, which can only be open using the secret code as the key.

*/

// The "user" parameter will contain the values of the user upon login.
module.exports.createAccessToken = (user) => {
    console.log(user);

    // payload of the JWT
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    // Generate a JSON Web Token using the jwt's "sign method".
    // Syntax:
        // jwt.sign(payload, secretOrPrivateKey, [options/callBackFunctions])
    return jwt.sign(data, secret, {});
};


// TOKEN VERIFICATION
/* 
    Analogy:
        - Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with
*/

// Middleware Function
module.exports.verify = (req, res, next) => {

    // The token is retrieved from the request header.
    let token = req.headers.authorization;

    // console.log(token);

    // if token is undefined, then req.headers.authorization is empty. Which means, the request did not pass a token in the authorization headers.
    if (token !== undefined){
        // res.send({message: "Token received!"});

        // The token sent is a type of "Bearer" which when received contains the "Bearer" as a prefix to the string. To remove the "Bearer" prefix we used the slice method to retrieve only the token.
        token = token.slice(7, token.length);

        console.log(token);

        // Validate the "token" using the "verify" method to decrypt the token using the secret code.
        // Syntax: jwt.verify(token, secretOrPrivateKey, [options/callbBackFunctions])

        return jwt.verify(token, secret, (error, data) => {
            // if jwt is not valid
            if (error) {
                return res.send({auth: "Invalid token!"});

            // if jwt is valid
            } else {
                // Allows the application to proceed with the next middleware function/callback function in the route.
                next();
            }
        })

    } else {
        res.send({message: "Auth failed. No token provided!"});
    }
};

// TOKEN DECRYPTION
/* 
    Analogy
        - Open the gift and get the content
*/

module.exports.decode = (token) => {
    if (token !== undefined) {
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (error, data) => {
            if (error) {
                // if token is not valid
                return null;
            } else {
                // decode method is used to obtain the information from the JWT
                // Syntax: jwt.decode(token, [options]);
                // Returns an object with access to the "payload" property which contains the user information stored when the token is generated.
                return jwt.decode(token, {complete:true}).payload;
            }
        })
    } else {
        // if token does not exist
        return null;
    }
};